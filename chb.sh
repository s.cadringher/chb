#!/bin/zsh
#    SYSTEM BRIGHTNESS CHANGER
#    Stefano Cadringher - 20/09/2018
#    s.cadringher@nacheso.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

MAX_BRIGHTNESS=$(cat /sys/class/backlight/intel_backlight/max_brightness)
ACTUAL_BRIGHTNESS=$(cat /sys/class/backlight/intel_backlight/actual_brightness)

function print 
{
  if [[ $2 != '-m' ]]
    then
      echo $1
  fi
}

if [[ $2 = 'on' ]];
  then
    #echo "baz"
    notify-send "automatic brightness: on" -t 2500
    (crontab -u root -l ; echo "* * * * * /usr/share/chb auto -m") | crontab -u root -
    exit;
  elif [[ $2 = 'off' ]];
    then
    #echo "boz"
    notify-send "automatic brightness: off" -t 2500
    crontab -u root -l | grep -v '/usr/share/chb auto -m' | crontab -u root -
    exit;
fi

if [ -z ${1} ];
  then 
    echo "\e[1mPC BRIGHTNESS CHANGER
(s.cadringher@nacheso.com)\n
\e[91mUSAGE:\e[39m\e[0m
chb\tN%\t: sets the percentage
   \t-m\t: no verbose
   \t+ \t: increase brightness
   \t- \t: decrease brightness
   \tauto\t: (on|off|set) auto brightness
";
    return -1; 
fi

#	CASO IN CUI SCELGO DI AUMENTARE
if [ $1 = '+' ]; 
  then 
  	let TOSET_INT=ACTUAL_BRIGHTNESS+500
#	CASO IN CUI SCELGO DI DIMINUIRE
elif [ $1 = '-' ]; 
  then
	  let TOSET_INT=ACTUAL_BRIGHTNESS-500
# LUMINOSITÀ AUTOMATICA
elif [ $1 = 'auto' ]; 
  then
    echo -ne "\e[101m\e[1mREADING...\e[0m\e[49m\033[0K\r"
    # fa una foto
    /usr/bin/notify-send "chainging brightness..." -t 2500
    fswebcam -q -r 640x480 --jpeg 85 -D 2.5 ~/tmp.jpg
    # prendi la luminanza della foto
    BRIGHTNESS=$( convert ~/tmp.jpg -colorspace Gray -format "%[mean]" info: )
    PERC=$(( (BRIGHTNESS * 100) / 65535 ))
    PERC_INT=${PERC%\.*}
    # aumento un po' dai
    if [ $PERC_INT -lt 90 ];
      then
        let PERC=PERC+10
    fi
    TOSET_FLOAT=$( bc -l <<< (($PERC / 100) * $MAX_BRIGHTNESS) )
    # A ME SERVE L'INTERO
    TOSET_INT=${TOSET_FLOAT%%.*}
    rm ~/tmp.jpg
#	CASO IN CUI METTO LA PERC
else
	case $1 in
	    ''|*[!0-9]*) exit ;;
	    *) print "[OK]" $2 ;;
	esac
	PERC=$1
	if [ $PERC -lt 0 ] || [ $PERC -gt 100 ];
	  then
	    print "\e[101mERR:\e[49m vINVALID VALUE." $2
	    PERC=100
	fi
	#	BASH GESTISCE SOLO I NUMERI INTERI, LOL
	#	AFFIDO A bc IL COMPITO DI DIVIDERE SERIAMENTE IL NUMERO
	TOSET_FLOAT=$( bc -l <<< (($PERC / 100) * $MAX_BRIGHTNESS) )
	#	A ME SERVE L'INTERO
	TOSET_INT=${TOSET_FLOAT%%.*}
fi

print "MAX_BRIGHTNESS      : $MAX_BRIGHTNESS" $2
print "ACTUAL_BRIGHTNESS   : "$ACTUAL_BRIGHTNESS $2
print "TARGET_BRIGHTNESS   : "$TOSET_INT $2

#	PARTE DIVERTENTE
#	IMPOSTO IL DELTA TRA LE DUE LUMINOSITÀ
if [ $ACTUAL_BRIGHTNESS -gt $TOSET_INT ];
  then
    mode=1
    print "\e[1mdecreasing brightness...\e[0m" $2
    DELTA=$(( $ACTUAL_BRIGHTNESS - $TOSET_INT ));
  else
    mode=2
    print "\e[1mincreasing brightness...\e[0m" $2
    DELTA=$(( $TOSET_INT - $ACTUAL_BRIGHTNESS ));
fi

#	IMPOSTO UNO STEP DI 1/100
step=$(( $DELTA / 100 ))
for i in `seq 1 100`;
  do
    if [ $mode -eq 2 ];
      then
        let ACTUAL_BRIGHTNESS=ACTUAL_BRIGHTNESS+step;
      else
        let ACTUAL_BRIGHTNESS=ACTUAL_BRIGHTNESS-step;
    fi
    PERCENTAGE=$(( ACTUAL_BRIGHTNESS*100 /  MAX_BRIGHTNESS ))
    if [[ $ACTUAL_BRIGHTNESS -lt 4882 ]]
      then
      	echo -ne "$PERCENTAGE%\033[0K\r"
        echo $ACTUAL_BRIGHTNESS > "/sys/class/backlight/intel_backlight/brightness"
      else
      	echo -ne "$MAX\033[0K\r"
    fi
    sleep .003
done
echo "\n"
/usr/bin/notify-send "brightness set to "$PERCENTAGE"%" -t 2500