# zsh brightness changer

<h1>PC BRIGHTNESS CHANGER</h1>
<tt>
USAGE:<br>
chb N%      : sets the percentage<br>
    -m      : no verbose<br>
    +       : increase brightness<br>
    -       : decrease brightness<br>
    auto     : (on|off|set) auto brightness</tt>
    
    
<h2>DEPENDENCIES</h2>
This script needs zsh, imagemagick, fswebcam and notify-send to work